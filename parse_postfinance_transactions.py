#!/usr/bin/env python
import math
import string
import sys
import re

#README: This is to parse the transactions list obtained by going to the E-Cockpit, viewing only the PFM frame, and saving the page source, then trimming out the main table, then running it through sed 's/</\n</g'

mode="all" #"credit" #"checking"

row_begin_re = re.compile("<tr data-id=\"([-0-9]*)\".*")

date_begin_re = re.compile("<td data-col=\"Date\".*")
date_re = re.compile(".*([0-9][0-9]\.[0-9][0-9]\.20[0-9][0-9]).*")
date_end_re = re.compile("</td>.*")
description_begin_re = re.compile("<td data-col=\"Description\".*")
description_re = re.compile("<span.*aria-label=\"([^\"]*)\".*>.*")
description_end_re = re.compile("</td>.*")
category_begin_re = re.compile("<td data-col=\"Category\".*")
category1_re = re.compile("<span>(.*) /")
category2_re = re.compile("<span>(.*[^/])\n")
category_end_re = re.compile("</td>.*")
amount_begin_re = re.compile("<td data-col=\"Amount\".*")
amount_re = re.compile("<span class=\"sf-.*amount.*>([\.'0-9]*[-+]).*")
amount_end_re = re.compile("</td>.*")

class Tx:
    txid=""
    date="1900-01-01"
    description="UNKNOWN"
    category="UNKNOWN"
    subcategory="UNKNOWN"
    debit=""
    credit=""

    def __init__(self):
        txid=""
        date="1900-01-01"
        description="UNKNOWN"
        category="UNKNOWN"
        subcategory="UNKNOWN"
        debit=""
        credit=""

    def print_csv(self):
        global mode
        if(mode == "all"):
            print "\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\"".format(self.txid, self.date, self.description, self.category, self.subcategory, self.debit, self.credit)
        else:
            if(mode == "checking" and self.txid[len(self.txid)-1] == "4"): #a wild assumption on the author's part that his checking account ending in "-4" means others may as well.  this is likely untrue, though.  modify it to suit your own needs
                print "\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\"".format(self.txid, self.date, self.description, self.category, self.subcategory, self.debit, self.credit)
            elif(mode == "credit" and self.txid[len(self.txid)-1] == "1"):
                print "\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\",\"{:s}\"".format(self.txid, self.date, self.description, self.category, self.subcategory, self.debit, self.credit)

def parse_row_line(tx, line):
    row_match = row_begin_re.match(line)
    if(row_match != None):
        tx.txid = row_match.group(1)

def parse_date_line(tx, line):
    date_match = date_re.match(line)
    if(date_match != None):
        date = date_match.group(1)
        date_parse_re = re.compile("([0-9]*)\.([0-9]*)\.([0-9]*)")
        date_parse_match = date_parse_re.match(date)
        if(date_parse_match == None):
            sys.stderr.write("Malformed date!")
            sys.exit(2)

        #now mangle the proper European-style date to match US-style software expectations.  FIXME remove when no longer needed
        day = int(date_parse_match.group(1))
        month = int(date_parse_match.group(2))
        year = int(date_parse_match.group(3))
        reformed_date = "{:d}-{:d}-{:d}".format(month, day, year)
        tx.date = reformed_date

def parse_description_line(tx, line):
    description_match = description_re.match(line)
    if(description_match != None):
        tx.description = description_match.group(1)

def parse_category_line(tx, line):
    category1_match = category1_re.match(line)
    category2_match = category2_re.match(line)
    if(category1_match != None):
        tx.category = category1_match.group(1)
    if(category2_match != None):
        tx.subcategory = category2_match.group(1)

def parse_amount_line(tx, line):
    amount_match = amount_re.match(line)
    if(amount_match != None):
        amount_str = string.replace(amount_match.group(1), '\'', '')
        sign = amount_str[len(amount_str)-1]
        if(sign == "-"):
            tx.debit = "{:s}".format(amount_str[0:len(amount_str)-1])
        else:
            tx.credit = "{:s}".format(amount_str[0:len(amount_str)-1])

def main():
    global mode

    if(len(sys.argv) > 1):
        mode = sys.argv[1]
    if(mode != "all" and mode != "checking" and mode != "credit"):
        sys.stderr.write("Bad mode provided!\n")
        sys.exit(3)

    #Implement a simple state machine to read through the file in chunks
    state = "INIT" #INIT, ROW, PREDATE, DATE, PREDESCR, DESCR, PRECAT, CAT, PREAMT, AMT
    curtx = Tx()
    while True:
        line = sys.stdin.readline()
        if not line:
            return 0

#        print "In state \"{:s}\", read line: \"{:s}\"".format(state, line) #DEBUG
        #First handle transitions:
        if state == "INIT":
            if(row_begin_re.match(line) != None):
#                print "TO ROW" #DEBUG
                state = "ROW"
            else:
                state = "INIT"
        elif state == "ROW":
            if(date_begin_re.match(line) != None):
#                print "TO DATE" #DEBUG
                state = "DATE"
            else:
                state = "PREDATE"
        elif state == "PREDATE":
            if(date_begin_re.match(line) != None):
#                print "TO DATE" #DEBUG
                state = "DATE"
            else:
                state = "PREDATE"
        elif state == "DATE":
            if(date_end_re.match(line) != None):
#                print "TO PREDESCR" #DEBUG
                state = "PREDESCR"
            else:
                state = "DATE"
        elif state == "PREDESCR":
            if(description_begin_re.match(line) != None):
#                print "TO DESCR" #DEBUG
                state = "DESCR"
            else:
                state = "PREDESCR"
        elif state == "DESCR":
            if(description_end_re.match(line) != None):
#                print "TO PRECAT" #DEBUG
                state = "PRECAT"
            else:
                state = "DESCR"
        elif state == "PRECAT":
            if(category_begin_re.match(line) != None):
#                print "TO CAT" #DEBUG
                state = "CAT"
            else:
                state = "PRECAT"
        elif state == "CAT":
            if(category_end_re.match(line) != None):
#                print "TO PREAMT" #DEBUG
                state = "PREAMT"
            else:
                state = "CAT"
        elif state == "PREAMT":
            if(amount_begin_re.match(line) != None):
#                print "TO AMT" #DEBUG
                state = "AMT"
            else:
                state = "PREAMT"
        elif state == "AMT":
            if(amount_end_re.match(line) != None):
#                print "TO INIT" #DEBUG
                state = "INIT"
            else:
                state = "AMT"
        else:
            sys.stderr.write("UNKNOWN STATE ENETERED!")
            sys.exit(1)

        #Now do state actions:
        if state == "INIT":
            if(curtx.txid != ""):
                curtx.print_csv()
            curtx = Tx()
        elif state == "ROW":
            parse_row_line(curtx, line)
        elif state == "DATE":
            parse_date_line(curtx, line)
        elif state == "DESCR":
            parse_description_line(curtx, line)
        elif state == "CAT":
            parse_category_line(curtx, line)
        elif state == "AMT":
            parse_amount_line(curtx, line)

main()
