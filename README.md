What you will need
====
1. `python` and `sed`.  Modern Linux distributions should need no new software.  Macs *might* work out of the box.  Windows users are advised to install Cygwin and use the Cygwin shell.
2. This script/repo
3. Reasonable familiarity with the developer mode in your browser of choice (ctrl+shift+i in Firefox)
4. No fear of the command line ;)

Scraping PostFinance Transaction Data
====

1. Log in to your PostFinance account and navigate to the E-Cockpit.
2. Click on 'Transactions' or 'Display Transactions'.
3. Ensure all desired accounts are selected, and 'total time period' is chosen (or whatever time period you want).
4. Right click near the transactions, in the same frame, and select 'Show Only This Frame" (or the equivalent in your browser of choice)
5. Once the frame loads, open the developer console in your browser and inspect the "Show 10 more transactions" button.  At the time of writing, it's a button with a class "sf-transactions-load-more".  So, go to the JavaScript console, and enter `for(var i = 0; i < 10000; i++){ document.getElementsByClassName("sf-transactions-load-more")[0].click() }`.  Then press `Enter` to run it.  It should take some time, then all your transactions will be displayed.
6. Now, right click the page and "Save Page As..." to an HTML file.
7. Open a command line / shell and navigate to where the HTML file is.  Now pipe that HTML file through `sed`, like so: `cat "PFM - PostFinance :: Transactions.html" | sed 's/</\n</g' >transactions.html`.  This ensures every opening HTML tag starts on a new line, which simplifies parsing.
8. Open that HTML file in the editor of your choice.  Delete everything from the beginning up until and including the second `<tbody>`.  Then, search from there to find the closing `</tbody>`.  Delete it and everything that comes after it.  Save this as `transactions_formatted.html`.
9. Run the script to generate a CSV file you can import to GnuCash (or likely other accounting software): `python parse_postfinance_transactions.py all <transactions_formatted.html >transactions.csv`
10. Import `transactions.csv` into GnuCash, paying careful attention to the date format.